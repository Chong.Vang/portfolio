'use client';
import React, { useState, useTransition } from 'react';
import Image from 'next/image';
import TabButton from './tabButton';


const TAB_DATA = [
    {
        title: "Skills",
        id: "skills",
        content: (
            <ul className='list-disc pl-2'>
                <li>PostgreSQL</li>
                <li>React</li>
                <li>Python</li>
                <li>Javascript</li>
                <li>Docker</li>
                <li>Django</li>
            </ul>
        )
    },
    {
        title: "Education",
        id: "education",
        content: (
            <ul className='list-disc pl-2'>
                <li> Hack Reactor | Software Engineering Immersive Program </li>
                <li> California State University | Sacramento </li>
                <li> Butte College </li>
                <li> Coastal Carolina Community College </li>
            </ul>
        )
    },
    {
        title: "Certifications",
        id: "certifications",
        content: (
            <ul className='list-disc pl-2'>
                <li> Software Engineering Certificate | Hack Reactor </li>
                <li> Associate Degree | Mathematics </li>
                <li> NDG Linux Certification | Cisco </li>
            </ul>
        )
    },
]

const AboutSection = () => {
    const [ tab, setTab ] = useState("skills");
    const [ isPending, startTransition] = useTransition();

    const handleTabChange = (id) => {
        startTransition(() => {
            setTab(id);
        });
    };

  return (
    <section className='text-white' id="about">
        <div className='md:grid md:grid-cols-2 gap-8 items-center py-8 px-4 xl:gap-16 sm:py-16 xl:px-16'>
            <Image
                src='/images/portrait.png'
                alt='portrait'
                width={700}
                height={700}
            />
            <div className='mt-4 md:mt-0 text-left flex flex-col h-full bg-[#121212] bg-opacity-75'>
                <h2 className='text-4xl font-bold text-white mb-4'>About Me</h2>
                <p className='text-base lg:text-lg text-justify'>
                    Greetings! I am an enthusiastic and driven software engineer and web developer with an unwavering passion for technology.
                    Since my earliest encounters with this ever-evolving industry, I have been captivated by its potential to transform our world for the better.
                    <br></br>
                    During my service in the United States Marine Corps, my fascination with the field of mechanical engineering surged, particularly during my exposure to aircraft.
                    I further supplemented this passion by enrolling in the Mechanical Engineering program at California State University. As I progressed in my academic journey,
                    my enthusiasm for mechanical engineering evolved into a strong interest in software engineering, driven by my enjoyment of coursework related to coding.
                    <br></br>
                    Inspired by producing solutions to real-world challenges as a software engineer, I am ecstatic to display the skillset that I possess for future opportunities in the
                    tech industry. With a solid foundation in coding, problem-solving, and a deep understanding of emerging technologies, I am confident that I can make meaningful
                    contributions to the evolving landscape of technology.
                    <br></br>
                </p>
                <div className='flex flex-row justify-start mt-8'>
                    <TabButton selectTab={() => handleTabChange("skills")} active={tab === "skills"}>
                        {""}
                        Skills{""}
                    </TabButton>
                    <TabButton selectTab={() => handleTabChange("education")} active={tab === "education"}>
                        {""}
                        Education{""}
                    </TabButton>
                    <TabButton selectTab={() => handleTabChange("certifications")} active={tab === "certifications"}>
                        {""}
                        Certifications{""}
                    </TabButton>
                </div>
                <div className='mt-8'>{TAB_DATA.find((t) => t.id === tab).content}
                </div>
            </div>
        </div>
    </section>
  )
}

export default AboutSection;